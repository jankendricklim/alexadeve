/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DBcon;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author Leif
 */
public class MySQLConnection extends ConnectionFactory {

    @Override
    public Connection getConnection() {
        try {
            Context ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup(getDataSourceName());
            Connection con = ds.getConnection();
            return con;
        } catch (SQLException sQLException) {
            sQLException.printStackTrace();
        } catch (NamingException ex) {
            Logger.getLogger(MySQLConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
