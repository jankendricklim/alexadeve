<%-- 
    Document   : CreateClass
    Created on : 10 5, 15, 8:36:53 PM
    Author     : Alexandria Ramos
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
    </body>
</html>
-->

<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Create Class</title>

        <!-- Bootstrap Core CSS -->
        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- MetisMenu CSS -->
        <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="dist/css/sb-admin-2.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="dist/css/test.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">

            <jsp:include page="Navigation.jsp" /> 

            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row"> <!-- header -->
                        <div class="col-lg-12">
                            <h1 class="page-header" align = "center	" >Create Class</h1>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form role="form" action="createClass">
                                    <!-- panel contents -->
                                    <div class="form-group">
                                        <label for="className"> Class Name:</label>
                                        <input  type="text" name="className" class="form-control" id="cName" placeholder="Enter Name of Subject here">
                                    </div>

                                    <div class="form-group">
                                        <label for="classCode"> Class Code:</label>
                                        <input type="text" name="classCode" class="form-control" id="cCode" placeholder="Enter Course Code here">
                                    </div>	

                                    <div class="form-group">
                                        <label for="cDescription"> Course Description:</label>
                                        <p> Enter Course Description here</p>
                                        <textarea class="form-control" rows="5" id="cDescription" name="classDesc"></textarea>
                                    </div>

                                    <label> Class Schedule</label>
                                    <p> Enter the day and timeslot of your class by clicking on the corresponding buttons below</p>
                                    <div class="form-group">
                                        <label for="day">Day</label>
                                        <select class="form-control">
                                            <option value="Monday">Monday</option>
                                            <option value="Tuesday">Tuesday</option>
                                            <option value="Wednesday">Wednesday</option>
                                            <option value="Thursday">Thursday</option>
                                            <option value="Friday">Friday</option>
                                            <option value="Saturday">Saturday</option>
                                            <option value="TBA">To Be Announced</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="timeslot">Timeslot</label>
                                        <select class="form-control">
                                            <option value="7:30 - 9:00 AM">7:30 - 9:00 AM</option>
                                            <option value="9:15 - 10:45 AM">9:15 - 10:45 AM</option>
                                            <option value="11:00 - 12:30 PM">11:00 - 12:30 PM</option>
                                            <option value="12:45 - 2:15 PM">12:45 - 2:15 PM</option>
                                            <option value="2:30 - 4:00 PM">2:30 - 4:00 PM</option>
                                            <option value="4:15 - 5:45 PM">4:15 - 5:45 PM</option>
                                            <option value="6:00 - 7:30 PM">6:00 - 7:30 PM</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="Venue">Venue</label>
                                        <select class="form-control">
                                            <option value="Gokongwei">Gokongwei</option>
                                            <option value="Bro. Andrew Gonzales Hall">Bro. Andrew Gonzales Hall</option>
                                            <option value="Mutien Marie">Mutien Marie</option>
                                            <option value="Yuchengco">Yuchengco</option>
                                            <option value="La Salle Hall">La Salle Hall</option>
                                            <option value="Miguel">Miguel</option>
                                            <option value="STRC">STRC</option>
                                            <option value="Velasco">Velasco</option>
                                        </select>
                                    </div>


                                        <input type="saveClass" class="btn btn-info" value="Save Class"> 





                                </form>
                            </div>
                        </div>
                    </div> <!-- /.container-fluid -->

                </div>        <!-- /#page-wrapper -->


            </div>
            <!-- /#wrapper -->

            <!-- jQuery -->
            <script src="bower_components/jquery/dist/jquery.min.js"></script>

            <!-- Bootstrap Core JavaScript -->
            <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

            <!-- Metis Menu Plugin JavaScript -->
            <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

            <!-- Custom Theme JavaScript -->
            <script src="dist/js/sb-admin-2.js"></script>

    </body>

</html>

