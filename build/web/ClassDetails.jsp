<%-- 
    Document   : Dashboard
    Created on : 10 5, 15, 8:51:21 PM
    Author     : Alexandria Ramos
--%>

<%@page import="GetSet.Class" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% Class classinfo = (Class) request.getAttribute("classInfo"); %>
<!DOCTYPE html>
<!--<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
    </body>
</html> -->

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>View Dashboard</title>

    <!-- Bootstrap Core CSS -->
    
    <jsp:include page="imports.jsp" /> 
    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <jsp:include page="Navigation.jsp" /> 

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"><%=classinfo.getClassName()%></h1>
                    </div>
                    <p>Class Description: <%=classinfo.getDescription() %></p>
                    <p>Class Date: <%=classinfo.getDate()%></p>
                    <p>Class Time: <%=classinfo.getTime()%></p>
                    <p>Handle Class: <%=classinfo.getHandleClass()%></p>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
  

</body>

</html>