<%@page import="java.util.ArrayList"%>
<%@page import="DAO.Dclass"%>
<%@page import="GetSet.Class"%>
<%@page import="GetSet.User"%>
<% User userinfo = (User)session.getAttribute("userDetails"); %>
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="Dashboard.html">Welcome to Collate</a>
    </div>


    <!-- /.navbar-header -->
    <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">

            <!-- envelope dropdown
<a class="dropdown-toggle" data-toggle="dropdown" href="#">
    <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
</a>
<ul class="dropdown-menu dropdown-messages">
    <li>
        <a href="#">
            <div>
                <strong>Timothy Kwok</strong>
                <span class="pull-right text-muted">
                    <em>September 23, 2015</em>
                </span>
            </div>
            <div>I love WEBDIST...</div>
        </a>
    </li>
    <li class="divider"></li>
    <li>
        <a href="#">
            <div>
                <strong>Jon Dimaculangan</strong>
                <span class="pull-right text-muted">
                    <em>Today</em>
                </span>
            </div>
            <div>Tara, kain?</div>
        </a>
    </li>
   
    <li class="divider"></li>
    <li>
        <a class="text-center" href="#">
            <strong>Read All Messages</strong>
            <i class="fa fa-angle-right"></i>
        </a>
    </li>
</ul>

</li>
            -->

            <!-- /.dropdown alerts/notifs -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-alerts">
                <li>
                    <a href="#">
                        <div>
                            <i class="fa fa-comment fa-fw"></i> New Comment 
                            <span class="pull-right text-muted small"> 20 minutes ago</span>
                        </div>
                    </a>
                </li>

                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <i class="fa fa-tasks fa-fw"></i> New Task
                            <span class="pull-right text-muted small">0 minutes ago</span>
                        </div>
                    </a>
                </li>

                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <i class="fa fa-tasks fa-fw"></i> New Task
                            <span class="pull-right text-muted small">4 minutes ago</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>

                <li class="divider"></li>
                <li>
                    <a class="text-center" href="#">
                        <strong>See All Alerts</strong>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </li>
            </ul>
            <!-- /.dropdown-alerts -->
        </li>


        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="Profile.html"><i class="fa fa-user fa-fw"></i> Profile</a>
                </li>
                <li class="divider"></li>
                <li><a href="LogIn.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->


    <!-- sidebar -->
    <div class="navbar-default sidebar" role="navigation">
        <img id="profile-img" class="profile-img-card" src="../pics/avatar_2x.png" />
        <p id="profile-name" class="profile-name-card"> Hi <%=userinfo.getFname()%>! </p>
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">

                <!-- Search -->
                <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                </li>

                <!-- Dashboard -->	
                <li>
                    <a href="Dashboard.jsp"><i class="fa fa-home fa-fw"></i> Dashboard </a>
                </li>

                <!-- Class Schedule -->		
                <li>
                    <a href="Profile.jsp#ClassSchedule"><i class="fa fa-calendar fa-fw"></i> My Class Schedule</a>
                </li>

                <!-- My Classes -->
                <li>
                    <a href="#"><i class="fa fa-align-justify fa-fw"></i> My Classes<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level"> <!-- /.nav-second-level -->
                        <% Dclass classesAvailable = new Dclass();
                            ArrayList<Class> Classes = new ArrayList<Class>();
                            if(userinfo.getUserType()==1) {
                                Classes = classesAvailable.getClasses();
                            } else {
                                Classes = classesAvailable.getClasses(userinfo.getGrpcode());
                            }
                            for (Class ClassInstance : Classes) {
                        %>
                        <li>
                            <a href="ClassInfo?ClassCode=<%=ClassInstance.getClassCode()%>"><%=ClassInstance.getClassName()%></a>
                        </li>
                        <% }%>
                    </ul>
                </li>

                <!-- Create Class -->
                <li>
                    <a href="CreateClass.jsp"><i class="fa fa-wrench fa-fw"></i> Create Class</a>
                </li>

            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>