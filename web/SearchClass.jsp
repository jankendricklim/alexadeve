<%-- 
    Document   : SearchClass
    Created on : 10 5, 15, 8:59:32 PM
    Author     : Alexandria Ramos
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
    </body>
</html>-->

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>View Dashboard</title>

    <!-- Bootstrap Core CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<link href="dist/css/test.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Welcome to Collate</a>
            </div>
			
			
            <!-- /.navbar-header -->
            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
				
				<!-- envelope dropdown
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="#">
                                <div>
                                    <strong>Timothy Kwok</strong>
                                    <span class="pull-right text-muted">
                                        <em>September 23, 2015</em>
                                    </span>
                                </div>
                                <div>I love WEBDIST...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>Jon Dimaculangan</strong>
                                    <span class="pull-right text-muted">
                                        <em>Today</em>
                                    </span>
                                </div>
                                <div>Tara, kain?</div>
                            </a>
                        </li>
                       
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
              
                </li>
				-->
				
                <!-- /.dropdown alerts/notifs -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-comment fa-fw"></i> New Comment 
                                    <span class="pull-right text-muted small"> 20 minutes ago</span>
                                </div>
                            </a>
                        </li>
						
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-tasks fa-fw"></i> New Task
                                    <span class="pull-right text-muted small">0 minutes ago</span>
                                </div>
                            </a>
                        </li>
						
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-tasks fa-fw"></i> New Task
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
				
				
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

			
			<!-- sidebar -->
            <div class="navbar-default sidebar" role="navigation">
			<img id="profile-img" class="profile-img-card" src="../pics/avatar_2x.png" />
			<p id="profile-name" class="profile-name-card"> Hi Alexa! </p>
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
					
					<!-- Search -->
					    <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
						
					<!-- Dashboard -->	
						<li>
                            <a href="Dashboard.html"><i class="fa fa-home fa-fw"></i> Dashboard </a>
                        </li>
						
					<!-- Class Schedule -->		
						<li>
                            <a href="Profile.html"><i class="fa fa-calendar fa-fw"></i> My Class Schedule</a>
                        </li>
                        
					<!-- My Classes -->
                        <li>
                            <a href="#"><i class="fa fa-align-justify fa-fw"></i> My Classes<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level"> <!-- /.nav-second-level -->
                                <li>
                                    <a href="index.html">2DGRAFX</a>
                                </li>
                                <li>
                                    <a href="index.html">WEBDEVE</a>
                                </li>
                                <li>
                                    <a href="index.html">ARCHROG</a>
                                </li>
                            </ul>
                        </li>
						
					<!-- Create Class -->
                    	<li>
                            <a href="#.html"><i class="fa fa-wrench fa-fw"></i> Create Class</a>
                        </li>
						
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Dashboard</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
				
				
				
				<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3>Class Schedule</h3>	</br>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead> <!-- Header of the table -->
                                        <tr> <!-- contents of 1st row -->
                                            <th></th>
											<th>
                                                Sunday
                                            </th>
                                            <th>
                                                Monday
                                            </th>
                                            <th>
                                                Tuesday
                                            </th>
											 <th>
                                                Wednesday
                                            </th>
                                            <th>
                                                Thursday
                                            </th>
											 <th>
                                                Friday
                                            </th>
                                            <th>
                                                Saturday
                                            </th>
                                        </tr>
                                    </thead>
									
                                    <tbody> <!-- Body of grid table -->
                                        <tr>
                                            <th> 7:30 - 9:00</th>
                                            <td>	</td>
                                            <!--<td colspan="3">Collapsed to start, horizontal above breakpoints</td>-->
											<td> WEBDEVE </td>
											<td>	</td>
											<td> WEBDEVE </td>
                                        </tr>
										<tr>
                                            <th> 9:15 - 10:45 </th>
                                        </tr>
										<tr>
                                            <th> 11:00 - 12:30 </th>
                                        </tr>
										<tr>
                                            <th> 12:45 - 2:15 </th>
                                        </tr>
										<tr>
                                            <th> 2:30 - 4:00 </th>
                                        </tr>
										<tr>
                                            <th> 4:15 - 5:45 </th>
                                        </tr>
										<tr>
                                            <th> 6:00 - 7:30 </th>
                                        </tr>
										<tr>
                                            <th> 7:45 - 9:15 </th>
                                        </tr>
										
										
										
										
										
                                    </tbody>
                                </table>
                            </div>
                            <p>Grid classes apply to devices with screen widths greater than or equal to the breakpoint sizes, and override grid classes targeted at smaller devices. Therefore, applying any
                                <code>.col-md-</code> class to an element will not only affect its styling on medium devices but also on large devices if a
                                <code>.col-lg-</code> class is not present.</p>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
				
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
