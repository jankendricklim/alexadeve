<%-- 
    Document   : newjsp
    Created on : 10 5, 15, 7:39:57 PM
    Author     : Alexandria Ramos
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<html ng-app="login">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <jsp:include page="imports.jsp" /> 
        <script type="text/javascript">
            var app = angular.module("login", []);
            app.controller("loginController", function($scope, $http){ 
                console.log('heller');
            });
            function init()
            {
                $('#TeachInfo').hide();
                $('#StudInfo').hide();
            }

            function onTeachClick() {
                $("#StudInfo").hide();
                $("#TeachInfo").show();
            }

            function onStudClick() {
                $("#StudInfo").show();
                $("#TeachInfo").hide();

            }

            $(document).ready(function() {
                $('#registerStudent').submit(function(e) {
                    e.preventDefault();
                    var data = {}
                    var Form = this;

                    //Gathering the Data
                    //and removing undefined keys(buttons)
                    $.each(this.elements, function(i, v) {
                        var input = $(v);
                        data[input.attr("name")] = input.val();
                        delete data["undefined"];
                    });

                    $.ajax({
                        url: "Registration",
                        type: "POST",
                        dataType: "json",
                        data: data,
                        success: function(data) {
                            if (!parseInt(data.responseCode)) {
                                $.ajax({
                                    url: "Loginz",
                                    type: "POST",
                                    dataType: "text/plain",
                                    data: {
                                        username: data.Userdetail.email,
                                        password: data.Userdetail.password
                                    }
                                });
                            } else {
                                alert(data.reponseMessage);
                            }
                        }
                    })

//                    $.ajax({
//                        url: $(this).attr('action') || window.location.pathname,
//                        type: "GET",
//                        data: $(this).serialize(),
//                        success: function(data) {
//                            $("#form_output").html(data);
//                        },
//                        error: function(jXHR, textStatus, errorThrown) {
//                            alert(errorThrown);
//                        }
//                    });
                });
            });

            function registerStudent(f) {
                console.log('here');
                $.ajax({
                    url: "Registration",
                    type: "POST",
                    dataType: "json",
                    data: JSON.stringify(f),
                    success: function(data) {
                        console.log(data.responseCode);

                    }
                })
            }

            function registerTeacher() {

            }


        </script>
    </head>
    <body ng-controller="loginController">

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Log In</h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="Loginz" role="form">
                                <fieldset>
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="user" placeholder="Username" name="username">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="pass" placeholder="Password" name="password">
                                    </div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <button type="submit" class="btn btn-default" action="Loginz">Sign in</button><br />
                                    <label>
                                        <a data-toggle="modal" data-target="#SignUp" onclick="init()"> Don't have an account </a> 
                                    </label>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="SignUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel" align="center">Sign-Up</h4>
                        <button type="button" class="btn btn-default btn-primary" onclick="onTeachClick()" id="Teachbtn">as a Teacher</button>
                        <button type="button" class="btn btn-default btn-primary" onclick="onStudClick()" id="Studbtn">as a Student</button>
                    </div>
                    <div class="modal-body">
                        <div id="TeachInfo">
                            <form role="form">
                                <div class="form-group" onsubmit="registerTeacher()">
                                    <label for="username">Group Code:</label>
                                    <input type="username" class="form-control" id="uname" placeholder="Enter group code">
                                    <p class="help-block">Username should contain letters and numbers without any spaces</p>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email:</label>
                                    <input type="email" class="form-control" id="email" placeholder="Enter email">
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Password:</label>
                                    <input type="password" class="form-control" id="psword" placeholder="Enter password">
                                    <p class="help-block">Password should be at least 6 characters</p>
                                </div>
                                <button type="submit" class="btn btn-default" data-dismiss="modal">Submit</button>
                            </form>
                        </div>

                        <div id="StudInfo">
                            <form role="form" id="registerStudent" name="registerStudentForm">
                                <div class="form-group" ng-class="{ 'has-error': registerStudentForm.fname.$invalid }" >
                                    <label for="firstname">First name:</label>
                                    <input type="text" class="form-control" id="firstname" name="fname" placeholder="Enter First name" ng-model="registerStudent.fname" required>
                                    <p class="help-block" ng-if="registerStudentForm.fname.$error.required">Required</p>
                                </div>
                                <div class="form-group">
                                    <label for="lastname">Last name:</label>
                                    <input type="text" class="form-control" id="lname" name="lname" placeholder="Enter Last name">
                                </div>
                                <div class="form-group">
                                    <label for="groupcode">Group Code:</label>
                                    <input type="text" class="form-control" id="groupcode" name="groupcode" placeholder="Enter Username">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email:</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Password:</label>
                                    <input type="password" class="form-control" id="psword" name="psword" placeholder="Enter password">
                                    <p class="help-block">Password should be at least 6 characters</p>
                                </div>
                                <input type="hidden" name="typ_acnt" value="1">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                    </div>
                </div>
            </div>
        </div>


    </body>
</html>
