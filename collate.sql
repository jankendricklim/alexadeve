-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: collate
-- ------------------------------------------------------
-- Server version	5.6.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id_Acnt` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `desc_Acnt` varchar(500) DEFAULT NULL,
  `fname_Acnt` varchar(20) NOT NULL,
  `lname_Acnt` varchar(15) NOT NULL,
  `email_Acnt` varchar(30) NOT NULL,
  `type_Acnt` int(1) NOT NULL,
  `pw_Acnt` varchar(30) NOT NULL,
  `grp_Code` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id_Acnt`),
  UNIQUE KEY `id_Account_UNIQUE` (`id_Acnt`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (00001,'I am hottie','Alexa','Ramos','Idol_Alexa@gmail.com',1,'idol',NULL),(00002,'Im IMBA','Jon','Dimaculangan','JC@yahoo.com',2,'profjon',NULL),(00003,'Average Student','Timothy','Kwok','timothykwok@gmail.com',1,'timtimtimtim',NULL),(00069,NULL,'Alain','Yhao','alain.com',2,'idol2',NULL),(00096,'','Alain','Yhao','alainyao.com',1,'idol2',NULL),(00097,NULL,'jan','lim','jan_kendrick_lim@dlsu.edu.ph',1,'janlim','037');
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class`
--

DROP TABLE IF EXISTS `class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class` (
  `ClassCode` varchar(10) NOT NULL,
  `name_Class` varchar(7) NOT NULL,
  `handle_Class` int(5) unsigned zerofill NOT NULL,
  `desc_Class` varchar(200) DEFAULT NULL,
  `date_Class` int(2) unsigned zerofill NOT NULL,
  `time_Class` int(1) unsigned zerofill NOT NULL,
  `grp_Code` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ClassCode`),
  UNIQUE KEY `id_ClassCode_UNIQUE` (`ClassCode`),
  KEY `email_Acnt_idx` (`handle_Class`),
  CONSTRAINT `handle_Class` FOREIGN KEY (`handle_Class`) REFERENCES `account` (`id_Acnt`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class`
--

LOCK TABLES `class` WRITE;
/*!40000 ALTER TABLE `class` DISABLE KEYS */;
INSERT INTO `class` VALUES ('ASAPXX2015','TREDTWO',00002,'dsadsa',06,567,'037'),('ASDFGHJKL;','CRSWARE',00002,'course ware for kids',13,1,'038'),('QWERTYUIOP','WEBDEVE',00002,'web development',24,4,'037'),('ZXCVBNM<>/','EDUMNGT',00002,'educational management system class',05,56,'037');
/*!40000 ALTER TABLE `class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classlist`
--

DROP TABLE IF EXISTS `classlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classlist` (
  `id_Acnt` int(5) unsigned NOT NULL,
  `ClassCode` varchar(10) NOT NULL,
  `name_Class` varchar(7) NOT NULL,
  PRIMARY KEY (`id_Acnt`),
  KEY `ClassCode_idx` (`ClassCode`),
  CONSTRAINT `id_Acnt` FOREIGN KEY (`id_Acnt`) REFERENCES `account` (`id_Acnt`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classlist`
--

LOCK TABLES `classlist` WRITE;
/*!40000 ALTER TABLE `classlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `classlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id_Comment` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_Post` int(10) unsigned zerofill NOT NULL,
  `comment` varchar(100) NOT NULL,
  PRIMARY KEY (`id_Comment`),
  UNIQUE KEY `id_Comment_UNIQUE` (`id_Comment`),
  KEY `id_Post_idx` (`id_Post`),
  CONSTRAINT `id_Post` FOREIGN KEY (`id_Post`) REFERENCES `post` (`id_Post`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id_Post` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `handle_Post` varchar(30) NOT NULL,
  `content_Post` varchar(500) NOT NULL,
  `ClassCode` varchar(10) NOT NULL,
  PRIMARY KEY (`id_Post`),
  UNIQUE KEY `id_Post_UNIQUE` (`id_Post`),
  KEY `ClassCode_idx` (`ClassCode`),
  CONSTRAINT `ClassCode` FOREIGN KEY (`ClassCode`) REFERENCES `class` (`ClassCode`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (0000000001,'Idol_Alexa@gmail.com','Do we have class tommorow????','ASDFGHJKL;'),(0000000002,'JC@yahoo,com','Yes we have class tommorow, Please review the ff items. 1. New teaching technologies, 2. Textbook based learning','ASDFGHJKL;'),(0000000003,'JC@yahoo.com','Muka kang tanga miss','ASDFGHJKL;');
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-29 18:07:54
