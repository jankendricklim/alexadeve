/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.Dusers;
import GetSet.RegistrationDetails;
import com.google.gson.*;
import GetSet.User;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Jon
 */
public class Registration extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Gson gson = new Gson();
        
        //BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
        User userdetails = new User();
        userdetails.setFname(request.getParameter("fname"));
        userdetails.setEmail(request.getParameter("email"));
        userdetails.setLname(request.getParameter("lname"));
        userdetails.setGrpcode(request.getParameter("groupcode"));
        userdetails.setPassword(request.getParameter("psword"));

        Dusers myDAO = new Dusers();
        RegistrationDetails regDetails = myDAO.register(userdetails, Integer.parseInt(request.getParameter("typ_acnt")));
        response.setContentType("application/json");
        // Get the printwriter object from response to write the required json object to the output stream      
        PrintWriter out = response.getWriter();
        // Assuming your json object is **jsonObject**, perform the following, it will return your json object  
        out.print(gson.toJson(regDetails));
        out.flush();
        /* String nextStep = "";
         HttpSession session = request.getSession();
         if (success){
         nextStep = "/ListUserz";
         session.setAttribute("login", "success");
         }else {
         nextStep = "/index.jsp";
         session.setAttribute("login", "failed");
         }
         ServletContext context = getServletContext();
         RequestDispatcher rd = context.getRequestDispatcher(nextStep);
         rd.forward(request, response); */
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
