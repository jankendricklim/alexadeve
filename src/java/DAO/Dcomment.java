/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DBcon.ConnectionFactory;
import GetSet.Comment;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Garfield
 */
public class Dcomment {
    public void createComment(Comment oneComment){ //create post
        try{
            ConnectionFactory myFactory = ConnectionFactory.getInstance();
            Connection con = myFactory.getConnection();
            
            String query = "insert into post (id_Post, comment, handle_Comment) values (?,?,?)";
            PreparedStatement ps = con.prepareStatement(query); //Statement s = con.createStatement();

            ps.setInt(1, oneComment.getPostid());
            ps.setString(2, oneComment.getComment());
            ps.setInt(3, oneComment.getHandleComment());
            ps.executeUpdate(query);	//executing statement

            con.close();	//closing connection
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void removeComment(int post, int handler)
    {
         try{
            ConnectionFactory myFactory = ConnectionFactory.getInstance();
            Connection con = myFactory.getConnection();
            
            String query = "DELETE FROM comment WHERE  id_Post = ? and handle_Comment = ? " ;
            PreparedStatement pstmt = con.prepareStatement(query);
            
          //  pstmt.setInt(1, post);
           // pstmt.setInt(2, handler);
            pstmt.executeUpdate(query);	//executing statement

            con.close();	//closing connection
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    	
    
}
