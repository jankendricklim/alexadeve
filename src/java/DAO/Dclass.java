/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import GetSet.Class;
import DBcon.ConnectionFactory;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Garfield
 */
public class Dclass {
    public void createClass(Class oneClass){
        try{
            ConnectionFactory myFactory = ConnectionFactory.getInstance();
            Connection con = myFactory.getConnection();
            
            String query = "insert into class (ClassCode, name_Class, handle_Class, desc_Class, date_Class, time_Class) values (?,?,?,?,?,?)";
            PreparedStatement ps = con.prepareStatement(query); //Statement s = con.createStatement();

            ps.setString(1, oneClass.getClassCode());
            ps.setString(2, oneClass.getClassName());
            ps.setInt(3, oneClass.getHandleClass());
            ps.setString(4, oneClass.getDescription());
            ps.setInt(5, oneClass.getDate());			
            ps.setInt(6, oneClass.getTime());
			
            //System.out.println(query);
            ps.executeUpdate(query);	//executing statement

            con.close();	//closing connection
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public ArrayList<Class> getClasses() {
        ArrayList<Class> Classes = new ArrayList<Class>();
        try{
            ConnectionFactory myFactory = ConnectionFactory.getInstance();
            Connection con = myFactory.getConnection();
            
            String query = "select * from class" ;
            PreparedStatement pstmt = con.prepareStatement(query);
			
            ResultSet rs = pstmt.executeQuery();	//executing statement
			//success = rs.next();
            while (rs.next())
            {
                Class classroom = new Class();
                classroom.setClassName(rs.getString("name_Class"));
                classroom.setClassCode(rs.getString("Classcode"));
                classroom.setDate(rs.getInt("date_class"));
                classroom.setTime(rs.getInt("time_class"));
                classroom.setHandleClass(rs.getInt("handle_class"));
                Classes.add(classroom);
            }			
            con.close();	//closing connection
        }catch(Exception e){
            e.printStackTrace();
        }
        return Classes;
    }
    
    
    public Class searchClass(String classname){ //search
        boolean success = false;
        Class classroom = new Class();
        try{
            ConnectionFactory myFactory = ConnectionFactory.getInstance();
            Connection con = myFactory.getConnection();
            
            String query = "select * from class where ClassCode = ?" ;
            PreparedStatement pstmt = con.prepareStatement(query);
            pstmt.setString(1, classname);
			
            
            ResultSet rs = pstmt.executeQuery();	//executing statement
            
			//success = rs.next();
            while (rs.next())
            {
                classroom.setClassName(rs.getString("name_Class"));
                classroom.setClassCode(rs.getString("Classcode"));
                classroom.setDate(rs.getInt("date_class"));
                classroom.setTime(rs.getInt("time_class"));
                classroom.setHandleClass(rs.getInt("handle_class"));
            }
			
            con.close();	//closing connection
        }catch(Exception e){
            e.printStackTrace();
        }
        return classroom;
    }    
    
	public void changeDesc(String desc, String classcode)
    {
        try{
            ConnectionFactory myFactory = ConnectionFactory.getInstance();
            Connection con = myFactory.getConnection();

            String query = " UPDATE class	SET name_Class = ? WHERE ClassCode = ? ";
            PreparedStatement ps = con.prepareStatement(query);
            
            ps.setString(1, desc);
            ps.setString(2, classcode);
            ps.executeUpdate(query);	//executing statement
            
        }catch(Exception e){
            e.printStackTrace();
        }
    }	
		
	public void changeClassName(String classname, String classcode)
    {
        try{
            ConnectionFactory myFactory = ConnectionFactory.getInstance();
            Connection con = myFactory.getConnection();

            String query = " UPDATE class	SET name_Class = ? WHERE ClassCode = ? ";
            PreparedStatement ps = con.prepareStatement(query);
            
            ps.setString(1, classname);
            ps.setString(2, classcode);
            ps.executeUpdate(query);	//executing statement
            
        }catch(Exception e){
            e.printStackTrace();
        }
    }	

	public void changeDate(int date, String classcode)
    {
        try{
            ConnectionFactory myFactory = ConnectionFactory.getInstance();
            Connection con = myFactory.getConnection();

            String query = " UPDATE class	SET date_Class = ? WHERE ClassCode = ? ";
            PreparedStatement ps = con.prepareStatement(query);
            
            ps.setInt(1, date);
            ps.setString(2, classcode);
            ps.executeUpdate(query);	//executing statement
            
        }catch(Exception e){
            e.printStackTrace();
        }
    }	

	public void changeTime(int time, String classcode)
    {
        try{
            ConnectionFactory myFactory = ConnectionFactory.getInstance();
            Connection con = myFactory.getConnection();

            String query = " UPDATE class	SET time_Class = ? WHERE ClassCode = ? ";
            PreparedStatement ps = con.prepareStatement(query);
            
            ps.setInt(1, time);
            ps.setString(2, classcode);
            ps.executeUpdate(query);	//executing statement
            
        }catch(Exception e){
            e.printStackTrace();
        }
    }	
	
    public void removeClass(String classcode, int handler)
    {
         try{
            ConnectionFactory myFactory = ConnectionFactory.getInstance();
            Connection con = myFactory.getConnection();
            
            String query = "DELETE FROM class WHERE ClassCode = ? and handle_Class = ? " ;
            PreparedStatement pstmt = con.prepareStatement(query);
            
        //    pstmt.setString(1, classcode);
         //   pstmt.setString(2, handler);
            pstmt.executeUpdate(query);	//executing statement

            con.close();	//closing connection
        }catch(Exception e){
            e.printStackTrace();
        }
    }
	
}
