/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DBcon.ConnectionFactory;
import GetSet.Post;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Garfield
 */
public class Dpost {
    public void createPost(Post onePost){ //create post
        try{
            ConnectionFactory myFactory = ConnectionFactory.getInstance();
            Connection con = myFactory.getConnection();
	
            String query = "insert into post (handle_Post, content_Post, ClassCode) values (?,?,?)";
            PreparedStatement ps = con.prepareStatement(query); //Statement s = con.createStatement();

            ps.setInt(1, onePost.getHandlePost());
            ps.setString(2, onePost.getContentPost());
            ps.setString(3, onePost.getClassCode());
            ps.executeUpdate(query);	//executing statement

            con.close();	//closing connection
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void removePost(int handler, String classcode)
    {
         try{
            ConnectionFactory myFactory = ConnectionFactory.getInstance();
            Connection con = myFactory.getConnection();

            String query = "DELETE FROM post WHERE handle_Post = ? and ClassCode = ? " ;
            PreparedStatement pstmt = con.prepareStatement(query);
            
            pstmt.setInt(1, handler);
            pstmt.setString(2, classcode);
            pstmt.executeUpdate(query);	//executing statement

            con.close();	//closing connection
        }catch(Exception e){
            e.printStackTrace();
        }
    }
		
    
}
