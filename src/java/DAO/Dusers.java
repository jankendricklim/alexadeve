/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import GetSet.User;
import DBcon.ConnectionFactory;
import GetSet.RegistrationDetails;
import com.google.gson.Gson;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Garfield
 */
public class Dusers {

    static final String grpCode = "01";
    static Random rnd = new Random();

    ConnectionFactory myFactory = ConnectionFactory.getInstance();
    Connection con = myFactory.getConnection();
    
    public String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(grpCode.charAt(rnd.nextInt(grpCode.length())));
        }
        return sb.toString();
    }
    //refer to DB check type_Acnt column

    public RegistrationDetails register(User oneUser, int type_Acnt) {

        //oneUser.setGrpcode(randomString(3));

        RegistrationDetails reg = new RegistrationDetails(oneUser, 0, "Success");

        if (type_Acnt == 1) //because student needs Grpcode upon registration
        {
            if (GroupCodeExists(oneUser.getGrpcode())) {
                try {
                    ConnectionFactory myFactory = ConnectionFactory.getInstance();
                    Connection con = myFactory.getConnection();
                    Gson gson = new Gson();
                    System.out.println(gson.toJson(oneUser).toString());
                    //String query = "insert into account (fname_Acnt, lname_Acnt, email_Acnt, type_Acnt, pw_Acnt, grp_Code) values (?,?,?,?,?,?,?);"; //,SHA1(CONCAT(?,'alexapulubi'))
                    //String query = "insert into account (fname_Acnt, lname_Acnt, email_Acnt, type_Acnt, pw_Acnt, grp_Code) values ("jan","lim","jan_kendrick_lim@dlsu.edu.ph",1,"janlim","037")
                    String query = "INSERT INTO account (fname_Acnt, lname_Acnt, email_Acnt, type_Acnt, pw_Acnt, grp_Code) VALUES (?,?,?,?,?,?);";
                    PreparedStatement ps = con.prepareStatement(query);	//creating statement					
                    ps.setString(1, oneUser.getFname());
                    ps.setString(2, oneUser.getLname());
                    ps.setString(3, oneUser.getEmail());
                    ps.setInt(4, oneUser.getUserType());
                    ps.setString(5, oneUser.getPassword());
                    ps.setString(6, oneUser.getGrpcode());
                    System.out.println(ps);
                    //ps.execute();
                    if(ps.execute()){
                        reg.setReponseMessage("Success!");
                        reg.setResponseCode(0);
                    }//executing statement

                    //con.close();	//closing connection
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                reg.setReponseMessage("Group Code does not exist!");
                reg.setResponseCode(1);
            }
        } else if (type_Acnt == 2) {
            try {
                ConnectionFactory myFactory = ConnectionFactory.getInstance();
                Connection con = myFactory.getConnection();
                String query = "insert into account (fname_Acnt, lname_Acnt, email_Acnt, type_Acnt, pw_Acnt, grp_Code) values (?,?,?,?,?)"; //,SHA1(CONCAT(?,'alexapulubi'))
                PreparedStatement ps = con.prepareStatement(query);	//creating statement					
                ps.setString(1, oneUser.getFname());
                ps.setString(2, oneUser.getLname());
                ps.setString(3, oneUser.getEmail());
                ps.setInt(4, oneUser.getUserType());
                ps.setString(5, oneUser.getPassword());
                if(ps.execute()) {
                    reg.setReponseMessage("Success!");
                    reg.setResponseCode(0);
                }	//executing statement

                con.close();	//closing connection
            } catch (Exception e) {
                e.printStackTrace();
                reg.setReponseMessage(e.getMessage());
                reg.setResponseCode(2);
            }
        }

        return reg;
    }

    public boolean GroupCodeExists(String groupcode) {
        boolean success = false;
        try {
            ConnectionFactory myFactory = ConnectionFactory.getInstance();
            Connection con = myFactory.getConnection();

            String query = "select * from class where grp_Code = ?";
            
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, groupcode);
            ResultSet rs = ps.executeQuery();	//executing statement
            success = rs.next() ? true : false;
            return success;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return success;
    }

    public User login(User oneUser) {
        boolean success = false;
        try {
            String query = "select * from account where email_Acnt = ? and pw_Acnt = ?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, oneUser.getEmail());
            ps.setString(2, oneUser.getPassword());
            ResultSet rs = ps.executeQuery();	//executing statement
            while (rs.next()) {
                oneUser.setFname(rs.getString("fname_Acnt"));
                oneUser.setLname(rs.getString("lname_Acnt"));
                oneUser.setUserType(rs.getInt("type_Acnt"));
                oneUser.setGrpcode(rs.getString("grp_Code"));
                success = true;
            }
            con.close();	//closing connection
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success ? oneUser : null;
    }

    public void changePass(String email, String newPass) {
        try {
            ConnectionFactory myFactory = ConnectionFactory.getInstance();
            Connection con = myFactory.getConnection();

            String query = " UPDATE account SET pw_Acnt = ? WHERE email_Acnt = ? ";

            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, email);
            ps.setString(2, newPass);
            ps.executeUpdate(query);	//executing statement

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void changeEmail(String oldEmail, String fname, String lname, String newEmail) {
        try {
            ConnectionFactory myFactory = ConnectionFactory.getInstance();
            Connection con = myFactory.getConnection();

            String query = " UPDATE account	SET email_Acnt = ? WHERE fname_Acnt = ? and lname_Acnt = ? and email_Acnt = ? ";
            PreparedStatement ps = con.prepareStatement(query);

            ps.setString(1, newEmail);
            ps.setString(2, fname);
            ps.setString(3, lname);
            ps.setString(4, oldEmail);
            ps.executeUpdate(query);	//executing statement

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void changeName(String fname, String lname, String email) {
        try {
            ConnectionFactory myFactory = ConnectionFactory.getInstance();
            Connection con = myFactory.getConnection();

            String query = " UPDATE account SET fname_Acnt = ? , lname_Acnt = ? WHERE email_Acnt = ? ";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, fname);
            ps.setString(2, lname);
            ps.setString(3, email);
            ps.executeUpdate(query);	//executing statement

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void changeDesc(String desc, String email) {
        try {
            ConnectionFactory myFactory = ConnectionFactory.getInstance();
            Connection con = myFactory.getConnection();

            String query = " UPDATE account	SET desc_Acnt = ? WHERE email_Acnt = ? ";
            PreparedStatement ps = con.prepareStatement(query);

            ps.setString(1, desc);
            ps.setString(2, email);
            ps.executeUpdate(query);	//executing statement

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void searchUser(String email, String fname, String lname) {
        try {
            ConnectionFactory myFactory = ConnectionFactory.getInstance();
            Connection con = myFactory.getConnection();

            String query = "select * from account where fname_Acnt = ? and lname_Acnt = ? and email_Acnt = ?";
            PreparedStatement pstmt = con.prepareStatement(query);

            pstmt.setString(1, fname);
            pstmt.setString(2, lname);
            pstmt.setString(3, email);
            pstmt.executeUpdate(query);	//executing statement

            con.close();	//closing connection
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeUser(String email, String password) {
        try {
            ConnectionFactory myFactory = ConnectionFactory.getInstance();
            Connection con = myFactory.getConnection();

            String query = "DELETE FROM account WHERE email_Acnt = ? and pw_Acnt = ? ";
            PreparedStatement pstmt = con.prepareStatement(query);

            pstmt.setString(1, email);
            pstmt.setString(2, password);
            pstmt.executeUpdate(query);	//executing statement

            con.close();	//closing connection
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<GetSet.User> listUsers() {
        ArrayList<GetSet.User> result = null;
        try {
            ConnectionFactory myFactory = ConnectionFactory.getInstance();
            Connection con = myFactory.getConnection();

            String query = "select * from users";
            PreparedStatement pstmt = con.prepareStatement(query);
            ResultSet rs = pstmt.executeQuery();	//executing statement
            result = new ArrayList<GetSet.User>();
            GetSet.User oneUser;
            while (rs.next()) {
                oneUser = new GetSet.User();
                oneUser.setUserid(rs.getInt("userid"));
                oneUser.setEmail(rs.getString("username"));
                oneUser.setPassword(rs.getString("password"));
                result.add(oneUser);
            }
            con.close();
            return result;
            //closing connection
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
