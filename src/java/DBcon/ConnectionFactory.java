package DBcon;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
/**
 *
 * @author Leif
 */
public abstract class ConnectionFactory {

    
    //private String dataSourceName ="java:comp/env/jdbc/webdist";
  
    public static ConnectionFactory getInstance() {
        return new MySQLConnection();
    }

    public abstract Connection getConnection();

    public void closeConnection(Connection connection) {
        try {
            connection.close();
        } catch (Exception e) {
        }
    }

    public void closeResultSet(ResultSet resultSet) {
        try {
            resultSet.close();
        } catch (Exception e) {
        }
    }

    public void closeStatement(Statement statement) {
        try {
            statement.close();
        } catch (Exception e) {
        }
    }
    
//    public String getUrl() {
//        return url;
//    }
//
//    public String getUsername() {
//        return username;
//    }
//
//    public String getPassword() {
//        return password;
//    }
//
//    public String getDriver() {
//        return driver;
//    }
//    
//    public void setDataSourceName(String dataSourceName) {
//        this.dataSourceName = dataSourceName;
//    }
//
//    public String getDataSourceName() {
//        return dataSourceName;
//    }
    
}
