/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GetSet;

/**
 *
 * @author Jon
 */
public class Comment {
    private int commentid;
	private int postid;
	private String comment; 
	private int hComment; 

	
    public int getCommentserid() {
        return commentid;
    }

    public void setCommentserid(int commentid) {
        this.commentid = commentid;
    }

    public int getPostid() {
        return postid;
    }

    public void setPostid(int postid) {
        this.postid = postid;
    }	
	
    public int getHandleComment() {
        return hComment;
    }

    public void setHandleComment(int hComment) {
        this.hComment = hComment;
    }
	
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }    
}