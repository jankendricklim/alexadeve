/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GetSet;

/**
 *
 * @author Jon
 */
public class Post {
    private int postid; //
    private int hPost; //
	private String cPost; //
	private String cCode;


    public int getPostid() {
        return postid;
    }

    public void setPostid(int postid) {
        this.postid = postid;
    }
	
    public int getHandlePost() {
        return hPost;
    }

    
    public void setHandlePost(int hPost) {
        this.hPost = hPost;
    }
	
	public String getClassCode() {
        return cCode;
    }

    public void setClassCode(String cCode) {
        this.cCode = cCode;
    } 
 
    public String getContentPost() {
        return cPost;
    }

    public void setContentPost(String cPost) {
        this.cPost = cPost;
    } 
}
