/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GetSet;

/**
 *
 * @author Jon
 */
public class Class {
	private String ccode;
    private String cname;	
	private int hclass;
    private String desc;
    private int date;
    private int time;

    public Class(String ccode, String cname, int hclass, String desc, int date, int time) {
        this.ccode = ccode;
        this.cname = cname;
        this.hclass = hclass;
        this.desc = desc;
        this.date = date;
        this.time = time;
    }

    public Class() {
    }
    
    
    
    public String getClassCode() { //added class code
        return ccode;
    }
	
    public void setClassCode(String classCode) { //added class code
        this.ccode = classCode;
    }
	
    public int getHandleClass() { //added handle class
        return hclass;
    }
	
    public void setHandleClass(int hclass) {
        this.hclass = hclass;
    }
	
    public String getClassName() {
        return cname;
    }

    public void setClassName(String className) {
        this.cname = className;
    }
    
    public String getDescription() {
        return desc;
    }

    public void setDescription(String descr) {
        this.desc = descr;
    }
    
    public int getTime() {
        return time;
    }

    public void setTime(int times) {
        this.time = times;
    }
    
    public int getDate() {
        return date;
    }

    public void setDate(int dates) {
        this.date = dates;
    }
}
