/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GetSet;

/**
 *
 * @author Jan
 */
public class RegistrationDetails {
    private User Userdetail;
    private int responseCode;
    private String reponseMessage;

    public User getUserdetail() {
        return Userdetail;
    }

    public void setUserdetail(User Userdetail) {
        this.Userdetail = Userdetail;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getReponseMessage() {
        return reponseMessage;
    }

    public void setReponseMessage(String reponseMessage) {
        this.reponseMessage = reponseMessage;
    }

    public RegistrationDetails(User Userdetail, int responseCode, String reponseMessage) {
        this.Userdetail = Userdetail;
        this.responseCode = responseCode;
        this.reponseMessage = reponseMessage;
    }
    
    
}
