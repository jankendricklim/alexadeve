/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GetSet;

/**
 *
 * @author Jon
 */
public class User {

    private int userid;
    private String password;
    private String lname;
    private String fname;
    private String email;
    private int userType;
    private String desc;
    private String Grpcode;

    public User(int userid, String password, String lname, String fname, String email, int userType, String desc, String Grpcode) {
        this.userid = userid;
        this.password = password;
        this.lname = lname;
        this.fname = fname;
        this.email = email;
        this.userType = userType;
        this.desc = desc;
        this.Grpcode = Grpcode;
    }

    public User() {
    }

    
    
    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getGrpcode() {
        return Grpcode;
    }

    public void setGrpcode(String Grpcode) {
        this.Grpcode = Grpcode;
    }
    

}